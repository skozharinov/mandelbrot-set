﻿#ifndef RGBCOLOR_HPP
#define RGBCOLOR_HPP

#include <cstdint>

class HsvColor;

class RgbColor {
public:
  static constexpr RgbColor fromRGB32(const uint32_t &);
  static constexpr RgbColor fromRGB64(const uint64_t &);

  constexpr RgbColor();
  constexpr RgbColor(const float &, const float &, const float &);
  constexpr RgbColor(const RgbColor &);

  constexpr float red() const;
  constexpr void setRed(const float &);

  constexpr float green() const;
  constexpr void setGreen(const float &);

  constexpr float blue() const;
  constexpr void setBlue(const float &);

  constexpr uint32_t toRGB32() const;
  constexpr uint64_t toRGB64() const;

  constexpr RgbColor interpolate(const RgbColor &, float) const;

  constexpr operator HsvColor() const;

  constexpr RgbColor &operator=(const RgbColor &);

  constexpr bool operator==(const RgbColor &);
  constexpr bool operator!=(const RgbColor &);

private:
  float _red;
  float _green;
  float _blue;
};

#include "HsvColor.hpp"
#include <algorithm>
#include <cassert>

constexpr RgbColor RgbColor::fromRGB32(const uint32_t &rgb32) {
  return {((rgb32 >> 16) & 0xFF) / static_cast<float>(0xFF),
          ((rgb32 >> 8) & 0xFF) / static_cast<float>(0xFF),
          (rgb32 & 0xFF) / static_cast<float>(0xFF)};
}

constexpr RgbColor RgbColor::fromRGB64(const uint64_t &rgb64) {
  return {((rgb64 >> 16) & 0xFFFF) / static_cast<float>(0xFFFF),
          ((rgb64 >> 8) & 0xFFFF) / static_cast<float>(0xFFFF),
          (rgb64 & 0xFFFF) / static_cast<float>(0xFFFF)};
}

constexpr RgbColor::RgbColor() : RgbColor(0, 0, 0) {}

constexpr RgbColor::RgbColor(const float &red, const float &green,
                             const float &blue)
    : _red(red), _green(green), _blue(blue) {
  assert(red >= 0 && red <= 1);
  assert(green >= 0 && green <= 1);
  assert(blue >= 0 && blue <= 1);
}

constexpr RgbColor::RgbColor(const RgbColor &color)
    : RgbColor(color.red(), color.green(), color.blue()) {}

constexpr float RgbColor::red() const { return _red; }

constexpr void RgbColor::setRed(const float &red) {
  assert(red >= 0 && red <= 1);
  _red = red;
}

constexpr float RgbColor::green() const { return _green; }

constexpr void RgbColor::setGreen(const float &green) {
  assert(green >= 0 && green <= 1);
  _green = green;
}

constexpr float RgbColor::blue() const { return _blue; }

constexpr void RgbColor::setBlue(const float &blue) {
  assert(blue >= 0 && blue <= 1);
  _blue = blue;
}

constexpr uint32_t RgbColor::toRGB32() const {
  uint32_t redPart = static_cast<uint32_t>(red() * 0xFF) << 16;
  uint32_t greenPart = static_cast<uint32_t>(green() * 0xFF) << 8;
  uint32_t bluePart = static_cast<uint32_t>(blue() * 0xFF);
  return 0xFFFFFFFF & (redPart | greenPart | bluePart);
}

constexpr uint64_t RgbColor::toRGB64() const {
  uint64_t redPart = static_cast<uint64_t>(red() * 0xFFFF) << 32;
  uint64_t greenPart = static_cast<uint64_t>(green() * 0xFFFF) << 16;
  uint64_t bluePart = static_cast<uint64_t>(blue() * 0xFFFF);
  return 0xFFFFFFFFFFFFFFFF & (redPart | greenPart | bluePart);
}

constexpr RgbColor RgbColor::interpolate(const RgbColor &other,
                                         float fraction) const {
  return {(other.red() - red()) * fraction + red(),
          (other.green() - green()) * fraction + green(),
          (other.blue() - blue()) * fraction + blue()};
}

constexpr RgbColor::operator HsvColor() const {
  float max = std::max({red(), green(), blue()});
  float min = std::min({red(), green(), blue()});

  float hue = 0;

  if (max == min) {
    hue = 0;
  } else if (max == red() && green() >= blue()) {
    hue = (green() - blue()) / (max - min) / 6;
  } else if (max == red() && green() < blue()) {
    hue = ((green() - blue()) / (max - min) + 6.0f) / 6;
  } else if (max == green()) {
    hue = ((blue() - red()) / (max - min) + 2.0f) / 6;
  } else if (max == blue()) {
    hue = ((red() - green()) / (max - min) + 3.0f) / 6;
  }

  float sat = 0;

  if (max == 0) {
    sat = 0;
  } else {
    sat = 1 - min / max;
  }

  float val = max;

  return {hue, sat, val};
}

constexpr RgbColor &RgbColor::operator=(const RgbColor &color) {
  setRed(color.red());
  setGreen(color.green());
  setBlue(color.blue());
  return *this;
}

constexpr bool RgbColor::operator==(const RgbColor &color) {
  return red() == color.red() && green() == color.green() &&
         blue() == color.blue();
}

constexpr bool RgbColor::operator!=(const RgbColor &color) {
  return red() != color.red() || green() != color.green() ||
         blue() != color.blue();
}

#endif // RGBCOLOR_HPP

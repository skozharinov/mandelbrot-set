﻿#ifndef UINT64VALIDATOR_HPP
#define UINT64VALIDATOR_HPP

#include <QValidator>

class UInt64Validator : public QValidator {
public:
  UInt64Validator(QObject * = nullptr);
  UInt64Validator(uint64_t, uint64_t, QObject * = nullptr);
  ~UInt64Validator() override = default;

  State validate(QString &, int &) const override;
  void fixup(QString &) const override;

  uint64_t bottom() const;
  void setBottom(const uint64_t &);

  uint64_t top() const;
  void setTop(const uint64_t &);

  void setRange(const uint64_t &, const uint64_t &);

private:
  uint64_t _bottom;
  uint64_t _top;
};

#endif // UINT64VALIDATOR_HPP

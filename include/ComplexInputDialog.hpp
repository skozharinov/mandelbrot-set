﻿#ifndef COMPLEXINPUTDIALOG_HPP
#define COMPLEXINPUTDIALOG_HPP

#include "Complex.hpp"
#include "MandelsetController.hpp"
#include <QDialog>

class QDoubleValidator;
class QLineEdit;

class ComplexInputDialog : public QDialog {
  Q_OBJECT
public:
  ComplexInputDialog(QWidget * = nullptr);

  Complex<MandelsetController::Type> complex() const;
  void setComplex(const Complex<MandelsetController::Type> &);

protected:
  void showEvent(QShowEvent *) override;

protected slots:
  void accept() override;

private:
  Complex<MandelsetController::Type> _complex;
  QDoubleValidator *_realValidator;
  QDoubleValidator *_imaginaryValidator;
  QPushButton *_acceptButton;
  QLineEdit *_realLineEdit;
  QLineEdit *_imaginaryLineEdit;
};

#endif // COMPLEXINPUTDIALOG_HPP

#ifndef MANDELBROTSET_ZOOMABLELABEL_HPP
#define MANDELBROTSET_ZOOMABLELABEL_HPP

#include <QLabel>

class ZoomableLabel : public QLabel {
  Q_OBJECT

public:


protected:

  void resizeEvent(QResizeEvent *) override;
  void mouseDoubleClickEvent(QMouseEvent *) override;
  void mousePressEvent(QMouseEvent *) override;
  void mouseMoveEvent(QMouseEvent *) override;
  void mouseReleaseEvent(QMouseEvent *) override;

private:

  QPoint _source;
};

#endif // MANDELBROTSET_ZOOMABLELABEL_HPP

﻿#ifndef COMPLEX_HPP
#define COMPLEX_HPP

#include <type_traits>

template <class F> class Complex {
  static_assert(std::is_floating_point<F>(), "F isn't floating point");

public:
  constexpr Complex(F = F(), F = F());
  constexpr Complex(const Complex<F> &);

  constexpr F real() const;
  constexpr F imaginary() const;

  constexpr F norm() const;
  constexpr F magnitude() const;

  constexpr Complex<F> conjugate() const;

  constexpr void setReal(F);
  constexpr void setImaginary(F);
  constexpr void setComplex(F, F);

  constexpr bool almostEquals(const Complex<F> &);

  constexpr Complex<F> &square();
  constexpr Complex<F> squared() const;

  constexpr Complex<F> &operator=(const Complex<F> &);
  constexpr Complex<F> &operator+=(const Complex<F> &);
  constexpr Complex<F> &operator-=(const Complex<F> &);
  constexpr Complex<F> &operator*=(const Complex<F> &);
  constexpr Complex<F> &operator/=(const Complex<F> &);

  constexpr Complex<F> operator+() const;
  constexpr Complex<F> operator-() const;

  constexpr Complex<F> operator+(const Complex<F> &) const;
  constexpr Complex<F> operator-(const Complex<F> &) const;
  constexpr Complex<F> operator*(const Complex<F> &)const;
  constexpr Complex<F> operator/(const Complex<F> &) const;

  constexpr bool operator==(const Complex<F> &);
  constexpr bool operator!=(const Complex<F> &);

private:
  F _real;
  F _imaginary;
};

#include "Util.hpp"
#include <cmath>

template <class F>
constexpr Complex<F>::Complex(F real, F imaginary)
    : _real(real), _imaginary(imaginary) {}

template <class F>
constexpr Complex<F>::Complex(const Complex<F> &number)
    : _real(number.real()), _imaginary(number.imaginary()) {}

template <class F> constexpr F Complex<F>::real() const { return _real; }

template <class F> constexpr F Complex<F>::imaginary() const {
  return _imaginary;
}

template <class F> constexpr F Complex<F>::norm() const {
  return real() * real() + imaginary() * imaginary();
}

template <class F> constexpr F Complex<F>::magnitude() const {
  return std::sqrt(norm());
}

template <class F> constexpr Complex<F> Complex<F>::conjugate() const {
  return {real(), -imaginary()};
}

template <class F> constexpr void Complex<F>::setReal(F real) { _real = real; }

template <class F> constexpr void Complex<F>::setImaginary(F imaginary) {
  _imaginary = imaginary;
}

template <class F> constexpr void Complex<F>::setComplex(F real, F imaginary) {
  setReal(real);
  setImaginary(imaginary);
}

template <class F>
constexpr bool Complex<F>::almostEquals(const Complex<F> &number) {
  return Util::almostEquals(real(), number.real()) &&
         Util::almostEquals(imaginary(), number.imaginary());
}

template <class F> constexpr Complex<F> &Complex<F>::square() {
  setComplex((real() - imaginary()) * (real() + imaginary()),
             2 * real() * imaginary());
  return *this;
}

template <class F> constexpr Complex<F> Complex<F>::squared() const {
  return {(real() - imaginary()) * (real() + imaginary()),
          2 * real() * imaginary()};
}

template <class F>
constexpr Complex<F> &Complex<F>::operator=(const Complex<F> &number) {
  setComplex(number.real(), number.imaginary());
  return *this;
}

template <class F>
constexpr Complex<F> &Complex<F>::operator+=(const Complex<F> &number) {
  setComplex(real() + number.real(), imaginary() + number.imaginary());
  return *this;
}

template <class F>
constexpr Complex<F> &Complex<F>::operator-=(const Complex<F> &number) {
  setComplex(real() - number.real(), imaginary() - number.imaginary());
  return *this;
}

template <class F>
constexpr Complex<F> &Complex<F>::operator*=(const Complex<F> &number) {
  setComplex(real() * number.real() - imaginary() * number.imaginary(),
             real() * number.imaginary() + number.real() * imaginary());
  return *this;
}

template <class F>
constexpr Complex<F> &Complex<F>::operator/=(const Complex<F> &number) {
  F div =
      number.real() * number.real() + number.imaginary() * number.imaginary();
  setComplex((real() * number.real() + imaginary() * number.imaginary()) / div,
             (number.real() * imaginary() - real() * number.imaginary()) / div);
  return *this;
}

template <class F> constexpr Complex<F> Complex<F>::operator+() const {
  return {*this};
}

template <class F> constexpr Complex<F> Complex<F>::operator-() const {
  return {-real(), -imaginary()};
}

template <class F>
constexpr Complex<F> Complex<F>::operator+(const Complex<F> &number) const {
  return {real() + number.real(), imaginary() + number.imaginary()};
}

template <class F>
constexpr Complex<F> Complex<F>::operator-(const Complex<F> &number) const {
  return {real() - number.real(), imaginary() - number.imaginary()};
}

template <class F>
constexpr Complex<F> Complex<F>::operator*(const Complex<F> &number) const {
  return {real() * number.real() - imaginary() * number.imaginary(),
          real() * number.imaginary() + number.real() * imaginary()};
}

template <class F>
constexpr Complex<F> Complex<F>::operator/(const Complex<F> &number) const {
  F div =
      number.real() * number.real() + number.imaginary() * number.imaginary();
  return {(real() * number.real() + imaginary() * number.imaginary()) / div,
          (number.real() * imaginary() - real() * number.imaginary()) / div};
}

template <class F>
constexpr bool Complex<F>::operator==(const Complex<F> &number) {
  return real() == number.real() && imaginary() == number.imaginary();
}

template <class F>
constexpr bool Complex<F>::operator!=(const Complex<F> &number) {
  return real() != number.real() || imaginary() != number.imaginary();
}

#endif // COMPLEX_HPP

﻿#ifndef POINT_HPP
#define POINT_HPP

#include <cstdint>

class Point {
public:
  constexpr Point(int32_t = int32_t(), int32_t = int32_t());
  constexpr Point(const Point &);

  constexpr int32_t x() const;
  constexpr void setX(const int32_t &);

  constexpr int32_t y() const;
  constexpr void setY(const int32_t &);

  constexpr Point &operator=(const Point &);

  constexpr Point &operator+=(const Point &);
  constexpr Point &operator-=(const Point &);

  constexpr Point operator+(const Point &) const;
  constexpr Point operator-(const Point &) const;

  constexpr bool operator==(const Point &);
  constexpr bool operator!=(const Point &);

private:
  int32_t _x;
  int32_t _y;
};

constexpr Point::Point(int32_t x, int32_t y) : _x(x), _y(y) {}

constexpr Point::Point(const Point &point) : _x(point.x()), _y(point.y()) {}

constexpr int32_t Point::x() const { return _x; }

constexpr void Point::setX(const int32_t &x) { _x = x; }

constexpr int32_t Point::y() const { return _y; }

constexpr void Point::setY(const int32_t &y) { _y = y; }

constexpr Point &Point::operator=(const Point &point) {
  setX(point.x());
  setY(point.y());
  return *this;
}

constexpr Point &Point::operator+=(const Point &point) {
  setX(x() + point.x());
  setY(y() + point.y());
  return *this;
}

constexpr Point &Point::operator-=(const Point &point) {
  setX(x() - point.x());
  setY(y() - point.y());
  return *this;
}

constexpr Point Point::operator+(const Point &point) const {
  return {x() + point.x(), y() + point.y()};
}

constexpr Point Point::operator-(const Point &point) const {
  return {x() - point.x(), y() - point.y()};
}

constexpr bool Point::operator==(const Point &point) {
  return x() == point.x() && y() == point.y();
}

constexpr bool Point::operator!=(const Point &point) {
  return x() != point.x() || y() != point.y();
}

#endif // POINT_HPP

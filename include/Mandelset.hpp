﻿#ifndef MANDELSET_HPP
#define MANDELSET_HPP

#include "Complex.hpp"
#include "Point.hpp"
#include <cmath>
#include <cstdint>
#include <type_traits>

template <class F> class Complex;
class RgbColor;

template <class F> class Mandelset {
  static_assert(std::is_floating_point<F>(), "F isn't floating point");

public:
  static constexpr const F Escape = 256;

  constexpr Mandelset(Point, uint64_t, Complex<double>, uint16_t);
  constexpr Mandelset(const Mandelset &);

  constexpr Complex<F> scale(const Point &) const;
  constexpr Point scale(const Complex<F> &) const;

  constexpr uint16_t iterations(const Point &) const;
  constexpr uint16_t iterations(const Complex<F> &) const;

  constexpr float smoothIterations(const Point &) const;
  constexpr float smoothIterations(const Complex<F> &) const;

  constexpr Complex<F> center() const;
  constexpr void setCenter(const Complex<F> &);

  constexpr Point size() const;
  constexpr void setSize(const Point &);

  constexpr uint64_t zoom() const;
  constexpr void setZoom(const uint64_t &);

  constexpr uint16_t iterations() const;
  constexpr void setIterations(const uint16_t &);

private:
  Complex<F> _center;
  Point _size;
  uint64_t _zoom;
  uint16_t _iterations;
};

#include "RgbColor.hpp"
#include "Util.hpp"

template <class F>
constexpr Mandelset<F>::Mandelset(Point size, uint64_t zoom,
                                  Complex<double> center, uint16_t iterations)
    : _center(center), _size(size), _zoom(zoom), _iterations(iterations) {}

template <class F>
constexpr Mandelset<F>::Mandelset(const Mandelset &set)
    : Mandelset<F>(set.size(), set.zoom(), set.center(), set.iterations()) {}

template <class F>
constexpr Complex<F> Mandelset<F>::scale(const Point &point) const {
  Complex<F> offset((static_cast<F>(size().x()) - 2 * point.x()) / 2 / zoom(),
                    (2 * point.y() - static_cast<F>(size().y())) / 2 / zoom());
  return center() - offset;
}

template <class F>
constexpr Point Mandelset<F>::scale(const Complex<F> &c) const {
  Complex<F> o{c - center()};
  return {(size().x() - static_cast<uint32_t>(o.real() * 2 * zoom())) / 2,
          (static_cast<uint32_t>(o.imaginary() * 2 * zoom() - size().y())) / 2};
}

template <class F>
constexpr uint16_t Mandelset<F>::iterations(const Point &point) const {
  return iterations(scale(point));
}

template <class F>
constexpr uint16_t Mandelset<F>::iterations(const Complex<F> &c) const {
  F q = Util::square(c.real() - 0.25) + Util::square(c.imaginary());

  if (q * (q + (c.real() - 0.25)) <= 0.25 * Util::square(c.imaginary())) {
    return iterations();
  }

  if (Util::square(c.real() + 1) + Util::square(c.imaginary()) <= 0.0625) {
    return iterations();
  }

  uint16_t counter = 0;
  Complex<F> z(0, 0);

  while (counter < iterations() && z.norm() <= Util::square(Escape)) {
    z = z.squared() + c;
    ++counter;
  }

  return counter;
}

template <class F>
constexpr float Mandelset<F>::smoothIterations(const Point &point) const {
  return smoothIterations(scale(point));
}

template <class F>
constexpr float Mandelset<F>::smoothIterations(const Complex<F> &c) const {
  F q = Util::square(c.real() - 0.25) + Util::square(c.imaginary());

  if (q * (q + (c.real() - 0.25)) <= 0.25 * Util::square(c.imaginary())) {
    return iterations();
  }

  if (Util::square(c.real() + 1) + Util::square(c.imaginary()) <= 0.0625) {
    return iterations();
  }

  uint16_t counter = 0;
  Complex<F> z(0, 0);

  while (counter < iterations() && z.norm() <= Util::square(Escape)) {
    z = z.squared() + c;
    ++counter;
  }

  if (counter == iterations()) {
    return counter;
  }

  F modifier = std::log2(std::log(z.norm()) / (std::log(Util::square(Escape))));
  return std::max(0.0, counter - modifier);
}

template <class F> constexpr Complex<F> Mandelset<F>::center() const {
  return _center;
}

template <class F>
constexpr void Mandelset<F>::setCenter(const Complex<F> &center) {
  _center = center;
}

template <class F> constexpr uint64_t Mandelset<F>::zoom() const {
  return _zoom;
}

template <class F> constexpr void Mandelset<F>::setZoom(const uint64_t &zoom) {
  _zoom = zoom;
}

template <class F> constexpr Point Mandelset<F>::size() const { return _size; }

template <class F> constexpr void Mandelset<F>::setSize(const Point &size) {
  _size = size;
}

template <class F> constexpr uint16_t Mandelset<F>::iterations() const {
  return _iterations;
}

template <class F>
constexpr void Mandelset<F>::setIterations(const uint16_t &iterations) {
  _iterations = iterations;
}

#endif // MANDELSET_HPP

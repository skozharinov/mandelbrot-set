﻿#ifndef MANDELSETCONTROLLER_HPP
#define MANDELSETCONTROLLER_HPP

#include "Colorizer.hpp"
#include "Complex.hpp"
#include "Point.hpp"
#include <QMutex>
#include <QObject>

template <class T> class Mandelset;

class MandelsetController : public QObject {
public:
  using Type = double;

  static constexpr const Complex<Type> DefaultCenter{-0.75, 0};
  static constexpr const Point DefaultSize{100, 100};
  static constexpr const uint64_t DefaultZoom{100};
  static constexpr const uint16_t DefaultIterations{100};
  static constexpr const Colorizer::Palette DefaultPalette{
      Colorizer::Grayscale};
  static constexpr const bool DefaultSmooth = true;
  static constexpr const bool DefaultHistogram = true;

  MandelsetController(QObject * = nullptr);
  ~MandelsetController() override = default;

  void lock();
  void unlock();

  void save();
  void restore();

  Mandelset<Type> set();

  Complex<Type> center();
  void setCenter(const Complex<Type> &);

  Point size();
  void setSize(const Point &);

  uint64_t zoom();
  void setZoom(const uint64_t &);

  uint16_t iterations();
  void setIterations(const uint16_t &);

  Colorizer::Palette palette();
  void setPalette(const Colorizer::Palette &);

  bool smooth();
  void setSmooth(bool);

  bool histogram() const;
  void setHistogram(bool);

private:
  QMutex _mutex;
  Complex<Type> _center;
  Point _size;
  uint64_t _zoom;
  uint16_t _iterations;
  Colorizer::Palette _palette;
  bool _smooth;
  bool _histogram;

  Complex<Type> _savedCenter;
  Point _savedSize;
  uint64_t _savedZoom;
  uint16_t _savedIterations;
  Colorizer::Palette _savedPalette;
  bool _savedSmooth;
  bool _savedHistogram;
};

#endif // MANDELSETCONTROLLER_HPP

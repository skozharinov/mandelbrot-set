﻿#ifndef COLORIZER_HPP
#define COLORIZER_HPP

#include <cstdint>

class RgbColor;

class Colorizer {
public:
  enum Palette : uint8_t {
    Pure,
    Grayscale,
    GrayscaleR,
    Blue,
    Royal,
    Hue,
    HueR,
    StarR,
    Heart
  };

  constexpr Colorizer(Palette);
  constexpr Colorizer(const Colorizer &);

  constexpr RgbColor operator()(float) const;

private:
  const Palette _palette;

  template <class Color, uint32_t N>
  constexpr Color map(float, const float (&)[N], const Color (&)[N]) const;

  template <class Color, uint32_t N>
  constexpr Color cycle(float, const float (&)[N], const Color (&)[N],
                        float) const;

  constexpr RgbColor pure(float) const;
  constexpr RgbColor grayscale(float) const;
  constexpr RgbColor grayscaleR(float) const;
  constexpr RgbColor blue(float) const;
  constexpr RgbColor royal(float) const;
  constexpr RgbColor hue(float) const;
  constexpr RgbColor hueR(float) const;
  constexpr RgbColor starR(float) const;
  constexpr RgbColor heart(float) const;
};

#include "HsvColor.hpp"
#include "RgbColor.hpp"
#include "Util.hpp"

constexpr Colorizer::Colorizer(Colorizer::Palette palette)
    : _palette(palette) {}

constexpr Colorizer::Colorizer(const Colorizer &colorizer)
    : Colorizer(colorizer._palette) {}

constexpr RgbColor Colorizer::operator()(float fraction) const {
  assert(fraction >= 0 && fraction <= 1);

  switch (_palette) {
  case Pure:
    return pure(fraction);
  case Grayscale:
    return grayscale(fraction);
  case GrayscaleR:
    return grayscaleR(fraction);
  case Blue:
    return blue(fraction);
  case Royal:
    return royal(fraction);
  case Hue:
    return hue(fraction);
  case HueR:
    return hueR(fraction);
  case StarR:
    return starR(fraction);
  case Heart:
    return heart(fraction);
  }

  return {};
}

template <class Color, uint32_t N>
constexpr Color Colorizer::map(float fraction, const float (&positions)[N],
                               const Color (&colors)[N]) const {
  static_assert(N > 0, "Cannot map 0 colors");

  if (fraction < positions[0]) {
    return colors[0];
  }

  for (uint32_t i = 0; i < N - 1; i++) {
    if (fraction >= positions[i] && fraction < positions[i + 1]) {
      float position =
          (fraction - positions[i]) / (positions[i + 1] - positions[i]);
      return colors[i].interpolate(colors[i + 1], position);
    }
  }

  return colors[N - 1];
}

template <class Color, uint32_t N>
constexpr Color Colorizer::cycle(float fraction, const float (&positions)[N],
                                 const Color (&colors)[N], float cycle) const {
  double left = 0;
  double right = 1;
  double pos = static_cast<double>(fraction);

  for (uint32_t i = 0; i < 4; ++i) {
    double mid = left + (right - left) * static_cast<double>(cycle);

    if (pos <= mid) {
      right = mid;
      break;
    } else {
      left = mid;
    }
  }

  float position = static_cast<float>((pos - left) / (right - left));
  return map(position, positions, colors);
}

constexpr RgbColor Colorizer::pure(float fraction) const {
  if (Util::almostEquals(fraction, 1.0f)) {
    return {1, 1, 1};
  } else {
    return {0, 0, 0};
  }
}

constexpr RgbColor Colorizer::grayscale(float fraction) const {
  return map(fraction, {0.0f, 1.0f}, {RgbColor(0, 0, 0), RgbColor(1, 1, 1)});
}

constexpr RgbColor Colorizer::grayscaleR(float fraction) const {
  return cycle(fraction, {0.0f, 1.0f},
               {RgbColor::fromRGB32(0x000000), RgbColor::fromRGB32(0xffffff)},
               0.5);
}

constexpr RgbColor Colorizer::blue(float fraction) const {
  return map(fraction, {0.0f, 0.95f, 1.0f},
             {RgbColor::fromRGB32(0xffffff), RgbColor::fromRGB32(0x3f51b5),
              RgbColor::fromRGB32(0x03a9f4)});
}

constexpr RgbColor Colorizer::royal(float fraction) const {
  return map(fraction, {0.0f, 0.95f, 1.0f},
             {RgbColor::fromRGB32(0x000000), RgbColor::fromRGB32(0x4a148c),
              RgbColor::fromRGB32(0xffffff)});
}

constexpr RgbColor Colorizer::hue(float fraction) const {
  if (Util::almostEquals(fraction, 1.0f)) {
    return {1, 1, 1};
  } else {
    return map(
        fraction,
        {0.0f / 6, 1.0f / 6, 2.0f / 6, 3.0f / 6, 4.0f / 6, 5.0f / 6, 6.0f / 6},
        {HsvColor(6.0f / 6, 1, 1), HsvColor(5.0f / 6, 1, 1),
         HsvColor(4.0f / 6, 1, 1), HsvColor(3.0f / 6, 1, 1),
         HsvColor(2.0f / 6, 1, 1), HsvColor(1.0f / 6, 1, 1),
         HsvColor(0.0f / 6, 1, 1)});
  }
}

constexpr RgbColor Colorizer::hueR(float fraction) const {
  if (Util::almostEquals(fraction, 1.0f)) {
    return {1, 1, 1};
  } else {
    return cycle(
        fraction,
        {0.0f / 6, 1.0f / 6, 2.0f / 6, 3.0f / 6, 4.0f / 6, 5.0f / 6, 6.0f / 6},
        {HsvColor(6.0f / 6, 1, 1), HsvColor(5.0f / 6, 1, 1),
         HsvColor(4.0f / 6, 1, 1), HsvColor(3.0f / 6, 1, 1),
         HsvColor(2.0f / 6, 1, 1), HsvColor(1.0f / 6, 1, 1),
         HsvColor(0.0f / 6, 1, 1)},
        0.5f);
  }
}

constexpr RgbColor Colorizer::starR(float fraction) const {
  if (Util::almostEquals(fraction, 1.0f)) {
    return RgbColor::fromRGB32(0xffffff);
  } else {
    return cycle(fraction, {0.0f, 0.2f, 0.4f, 0.6f, 0.8f, 1.0f},
                 {RgbColor::fromRGB32(0x590e22), RgbColor::fromRGB32(0x0c0344),
                  RgbColor::fromRGB32(0x3489da), RgbColor::fromRGB32(0xffffff),
                  RgbColor::fromRGB32(0xffd22f), RgbColor::fromRGB32(0xae430d)},
                 0.7f);
  }
}

constexpr RgbColor Colorizer::heart(float fraction) const {
  return map(fraction, {0.3f, 0.99f, 1.0f},
             {RgbColor::fromRGB32(0x000000), RgbColor::fromRGB32(0xff0000),
              RgbColor::fromRGB32(0x000000)});
}

#endif // COLORIZER_HPP

﻿#ifndef MANDELSETWIDGET_HPP
#define MANDELSETWIDGET_HPP

#include "MandelsetController.hpp"
#include <QMainWindow>
#include <QMap>

class RenderThread;
class ZoomableLabel;

class QActionGroup;
class QLabel;
class QProgressDialog;

class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  explicit MainWindow(QWidget * = nullptr);
  ~MainWindow() override;

protected slots:
  void exportImage();
  void reset();
  void exit();
  void setCenter();
  void setZoom();
  void setIterations();
  void setPalette();
  void zoomIn();
  void zoomOut();
  void smooth();
  void histogram();
  void about();
  void aboutQt();

  void render();
  void zoomTo(Complex<MandelsetController::Type>, float);
  void showProgressDialog();
  void updateProgress(int);
  void updateLabels();
  void updatePixmap(const QPixmap &);

protected:
  void createActions();
  void createMenus();

private:
  MandelsetController *_controller;
  RenderThread *_renderThread;
  QProgressDialog *_progressDialog;
  QPixmap _pixmap;
  ZoomableLabel *_pixmapLabel;
  QLabel *_zoomLabel;

  QMenu *_fileMenu;
  QMenu *_editMenu;
  QMenu *_viewMenu;
  QMenu *_helpMenu;

  QActionGroup *_paletteGroup;
  QMap<Colorizer::Palette, QAction *> _paletteActions;

  QAction *_exportAction;
  QAction *_resetAction;
  QAction *_exitAction;
  QAction *_setCenterAction;
  QAction *_setZoomAction;
  QAction *_setIterationsAction;
  QAction *_zoomInAction;
  QAction *_zoomOutAction;
  QAction *_smoothAction;
  QAction *_histogramAction;
  QAction *_purePaletteAction;
  QAction *_grayscalePaletteAction;
  QAction *_grayscaleRPaletteAction;
  QAction *_bluePaletteAction;
  QAction *_royalPaletteAction;
  QAction *_huePaletteAction;
  QAction *_hueRPaletteAction;
  QAction *_heartPaletteAction;
  QAction *_starRPaletteAction;
  QAction *_aboutAction;
  QAction *_aboutQtAction;

  friend class ZoomableLabel;
};

#endif // MANDELSETWIDGET_HPP

﻿#ifndef UTIL_HPP
#define UTIL_HPP

#include <cstdint>
#include <string>

template <class F> class Complex;

class Util {
public:
  template <class F> static constexpr bool almostEquals(F, F);
  template <class F> static std::string toStdString(F, uint8_t = 8);
  template <class T> static constexpr T square(T);
};

#include "Complex.hpp"
#include <algorithm>
#include <limits>
#include <sstream>
#include <type_traits>

template <class F> constexpr bool Util::almostEquals(F first, F second) {
  static_assert(std::is_floating_point<F>(), "F isn't floating point");
  constexpr F epsilon = std::numeric_limits<F>::epsilon();
  return std::abs(first - second) < 10 * epsilon * std::max(first, second);
}

template <class F> std::string Util::toStdString(F number, uint8_t precision) {
  static_assert(std::is_floating_point<F>(), "F isn't floating point");
  std::ostringstream dummy;
  dummy.precision(precision);
  dummy << std::fixed << number;
  return dummy.str();
}

template <class T> constexpr T Util::square(T a) { return a * a; }

#endif // UTIL_HPP

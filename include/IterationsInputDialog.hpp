﻿#ifndef ITERATIONSINPUTDIALOG_HPP
#define ITERATIONSINPUTDIALOG_HPP

#include <QDialog>

class QLineEdit;

class UInt64Validator;

class IterationsInputDialog : public QDialog {
  Q_OBJECT
public:
  IterationsInputDialog(QWidget * = nullptr);

  uint16_t iterations() const;
  void setIterations(const uint16_t &);

protected:
  void showEvent(QShowEvent *) override;

protected slots:
  void accept() override;

private:
  UInt64Validator *_iterationsValidator;
  QPushButton *_acceptButton;
  QLineEdit *_iterationsLineEdit;
  uint16_t _iterations;
};

#endif // ITERATIONSINPUTDIALOG_HPP

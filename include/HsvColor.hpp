﻿#ifndef HSVCOLOR_HPP
#define HSVCOLOR_HPP

class RgbColor;

class HsvColor {
public:
  constexpr HsvColor();
  constexpr HsvColor(const float &, const float &, const float &);
  constexpr HsvColor(const HsvColor &);

  constexpr float hue() const;
  constexpr void setHue(const float &);

  constexpr float saturation() const;
  constexpr void setSaturation(const float &);

  constexpr float value() const;
  constexpr void setValue(const float &);

  constexpr HsvColor interpolate(const HsvColor &, float) const;

  constexpr operator RgbColor() const;

  constexpr HsvColor &operator=(const HsvColor &);

  constexpr bool operator==(const HsvColor &);
  constexpr bool operator!=(const HsvColor &);

private:
  float _hue;
  float _saturation;
  float _value;
};

#include "RgbColor.hpp"
#include <algorithm>
#include <cassert>
#include <cmath>

constexpr HsvColor::HsvColor() : HsvColor(0, 0, 0) {}

constexpr HsvColor::HsvColor(const float &hue, const float &saturation,
                             const float &value)
    : _hue(hue), _saturation(saturation), _value(value) {
  assert(hue >= 0 && hue <= 1);
  assert(saturation >= 0 && saturation <= 1);
  assert(value >= 0 && value <= 1);
}

constexpr HsvColor::HsvColor(const HsvColor &color)
    : HsvColor(color.hue(), color.saturation(), color.value()) {}

constexpr float HsvColor::hue() const { return _hue; }

constexpr void HsvColor::setHue(const float &hue) {
  assert(hue >= 0 && hue <= 1);
  _hue = hue;
}

constexpr float HsvColor::saturation() const { return _saturation; }

constexpr void HsvColor::setSaturation(const float &saturation) {
  assert(saturation >= 0 && saturation <= 1);
  _saturation = saturation;
}

constexpr float HsvColor::value() const { return _value; }

constexpr void HsvColor::setValue(const float &value) {
  assert(value >= 0 && value <= 1);
  _value = value;
}

constexpr HsvColor HsvColor::interpolate(const HsvColor &other,
                                         float fraction) const {
  float max = std::max(hue(), other.hue());
  float min = std::min(hue(), other.hue());

  if (max == hue()) {
    fraction = 1 - fraction;
  }

  float dist = max - min;
  float hue = 0;

  if (dist > 0.5f) {
    min = min + 1;
    hue = std::fmod(dist * fraction + min, 1.0f);
  } else if (dist <= 0.5f) {
    hue = dist * fraction + min;
  }

  float sat = (other.saturation() - saturation()) * fraction + saturation();
  float val = (other.value() - value()) * fraction + value();

  return {hue, sat, val};
}

constexpr HsvColor::operator RgbColor() const {
  float c = saturation() * value();
  float x = c * (1.0f - std::abs(std::fmod(360 * hue() / 60.0f, 2.0f) - 1.0f));
  float m = value() - c;

  float red = 0;
  float green = 0;
  float blue = 0;

  if (hue() >= 0 && hue() < 1.0f / 6) {
    red = c;
    green = x;
    blue = 0;
  } else if (hue() >= 1.0f / 6 && hue() < 2.0f / 6) {
    red = x;
    green = c;
    blue = 0;
  } else if (hue() >= 2.0f / 6 && hue() < 3.0f / 6) {
    red = 0;
    green = c;
    blue = x;
  } else if (hue() >= 3.0f / 6 && hue() < 4.0f / 6) {
    red = 0;
    green = x;
    blue = c;
  } else if (hue() >= 4.0f / 6 && hue() < 5.0f / 6) {
    red = x;
    green = 0;
    blue = c;
  } else {
    red = c;
    green = 0;
    blue = x;
  }

  return {red + m, green + m, blue + m};
}

constexpr HsvColor &HsvColor::operator=(const HsvColor &color) {
  setHue(color.hue());
  setSaturation(color.saturation());
  setValue(color.value());
  return *this;
}

constexpr bool HsvColor::operator==(const HsvColor &color) {
  return hue() == color.hue() && saturation() == color.saturation() &&
         value() == color.saturation();
}

constexpr bool HsvColor::operator!=(const HsvColor &color) {
  return hue() != color.hue() || saturation() != color.saturation() ||
         value() != color.saturation();
}

#endif // HSVCOLOR_HPP

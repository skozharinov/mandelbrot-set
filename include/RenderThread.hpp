﻿#ifndef RENDERTHREAD_HPP
#define RENDERTHREAD_HPP

#include <QMutex>
#include <QThread>

class MandelsetController;

class RenderThread : public QThread {
  Q_OBJECT
public:
  RenderThread(MandelsetController *, QObject * = nullptr);
  void render();

public slots:
  void restart();
  void abort();

signals:
  void rendered(const QPixmap &);
  void progress(int);

protected:
  void run() override;
  bool checkRestart();
  bool checkAbort();

private:
  MandelsetController *_controller;
  QMutex _mutex;
  bool _restart;
  bool _abort;
};

#endif // RENDERTHREAD_HPP

﻿#ifndef ZOOMINPUTDIALOG_HPP
#define ZOOMINPUTDIALOG_HPP

#include <QDialog>

class QLineEdit;

class UInt64Validator;

class ZoomInputDialog : public QDialog {
  Q_OBJECT
public:
  ZoomInputDialog(QWidget * = nullptr);

  uint64_t zoom() const;
  void setZoom(const uint64_t &);

protected:
  void showEvent(QShowEvent *) override;

protected slots:
  void accept() override;

private:
  uint64_t _zoom;
  UInt64Validator *_zoomValidator;
  QPushButton *_acceptButton;
  QLineEdit *_zoomLineEdit;
};

#endif // ZOOMINPUTDIALOG_HPP

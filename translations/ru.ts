<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>ComplexInputDialog</name>
    <message>
        <location filename="../src/ComplexInputDialog.cpp" line="17"/>
        <source>Enter</source>
        <translation>Ввод</translation>
    </message>
    <message>
        <location filename="../src/ComplexInputDialog.cpp" line="27"/>
        <source>Real</source>
        <translation>Действительная часть</translation>
    </message>
    <message>
        <location filename="../src/ComplexInputDialog.cpp" line="28"/>
        <source>Imaginary</source>
        <translation>Мнимая часть</translation>
    </message>
</context>
<context>
    <name>IterationsInputDialog</name>
    <message>
        <location filename="../src/IterationsInputDialog.cpp" line="17"/>
        <source>Enter</source>
        <translation>Ввод</translation>
    </message>
    <message>
        <location filename="../src/IterationsInputDialog.cpp" line="24"/>
        <source>Iterations</source>
        <translation>Итерации</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/MainWindow.cpp" line="66"/>
        <source>Export As</source>
        <translation>Экспортировать как</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="88"/>
        <source>Export Error</source>
        <translation>Ошибка экспорта</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="89"/>
        <source>The image could not be saved to &quot;%1&quot;.</source>
        <translation>Изображение не может быть сохранено в &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="223"/>
        <source>A school project that visualizes Mandelbrot set on complex plane.</source>
        <translation>Школьный проект, визуализирующий множество Мандельброта на комплексной плоскости.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="256"/>
        <source>Zoom: %1x</source>
        <translation>Приближение: %1x</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="39"/>
        <source>Rendering...</source>
        <translation>Отрисовка...</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="41"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="267"/>
        <source>Export</source>
        <translation>Экспортировать</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="272"/>
        <source>Reset</source>
        <translation>Сбросить</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="276"/>
        <source>Exit</source>
        <translation>Выйти</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="281"/>
        <source>Set Center</source>
        <translation>Выбрать центр</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="284"/>
        <source>Set Zoom</source>
        <translation>Выбрать приближение</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="287"/>
        <source>Set Iterations</source>
        <translation>Выбрать количество итераций</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="291"/>
        <source>Zoom In</source>
        <translation>Приблизить</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="296"/>
        <source>Zoom Out</source>
        <translation>Отдалить</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="301"/>
        <source>Smooth Coloring</source>
        <translation>Плавное раскрашивание</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="306"/>
        <source>Histogram Coloring</source>
        <translation>Использовать гистограмму</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="311"/>
        <source>Pure Palette</source>
        <translation>Простая палитра</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="321"/>
        <source>Grayscale R Palette</source>
        <translation>Серая R палитра</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="326"/>
        <source>Blue Palette</source>
        <translation>Голубая палитра</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="316"/>
        <source>Grayscale Palette</source>
        <translation>Серая палитра</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="331"/>
        <source>Royal Palette</source>
        <translation>Королевская палитра</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="336"/>
        <source>Hue Palette</source>
        <translation>Разноцветная палитра</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="341"/>
        <source>Hue R Palette</source>
        <oldsource>HueR Palette</oldsource>
        <translation>Разноцветная R палитра</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="346"/>
        <source>Star R Palette</source>
        <translation>Звёздная R палитра</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="351"/>
        <source>Heart Palette</source>
        <translation>Палитра Сердца</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="380"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="384"/>
        <source>About Qt</source>
        <translation>О Qt</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="389"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="396"/>
        <source>Edit</source>
        <translation>Редактировать</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="401"/>
        <source>View</source>
        <translation>Вид</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="409"/>
        <source>Palette</source>
        <translation>Палитра</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="415"/>
        <source>Help</source>
        <translation>Справка</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="428"/>
        <source>%1 x %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="456"/>
        <source>(%1; %2)</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../src/main.cpp" line="15"/>
        <source>Mandelbrot Set</source>
        <translation>Множество Мандельброта</translation>
    </message>
</context>
<context>
    <name>ZoomInputDialog</name>
    <message>
        <location filename="../src/ZoomInputDialog.cpp" line="16"/>
        <source>Enter</source>
        <translation>Ввод</translation>
    </message>
    <message>
        <location filename="../src/ZoomInputDialog.cpp" line="23"/>
        <source>Zoom</source>
        <translation>Приближение</translation>
    </message>
</context>
</TS>

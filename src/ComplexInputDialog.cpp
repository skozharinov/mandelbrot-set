﻿#include "ComplexInputDialog.hpp"

#include <QApplication>
#include <QDoubleValidator>
#include <QFormLayout>
#include <QLineEdit>
#include <QPushButton>

ComplexInputDialog::ComplexInputDialog(QWidget *parent) : QDialog(parent) {
  setWindowTitle(QApplication::applicationDisplayName());

  _realValidator = new QDoubleValidator(-2, 2, 16, this);
  _realValidator->setNotation(QDoubleValidator::StandardNotation);
  _imaginaryValidator = new QDoubleValidator(-2, 2, 16, this);
  _imaginaryValidator->setNotation(QDoubleValidator::StandardNotation);

  _acceptButton = new QPushButton(tr("Enter"));
  connect(_acceptButton, &QPushButton::clicked, this, &QDialog::accept);

  _realLineEdit = new QLineEdit();
  _realLineEdit->setValidator(_realValidator);

  _imaginaryLineEdit = new QLineEdit();
  _imaginaryLineEdit->setValidator(_imaginaryValidator);

  QFormLayout *layout = new QFormLayout();
  layout->addRow(tr("Real"), _realLineEdit);
  layout->addRow(tr("Imaginary"), _imaginaryLineEdit);
  layout->addRow(_acceptButton);

  setLayout(layout);
}

void ComplexInputDialog::showEvent(QShowEvent *event) {
  QDialog::showEvent(event);

  _realLineEdit->setText(
      QString::fromStdString(std::to_string(_complex.real())));
  _imaginaryLineEdit->setText(
      QString::fromStdString(std::to_string(_complex.imaginary())));
}

Complex<MandelsetController::Type> ComplexInputDialog::complex() const {
  return _complex;
}

void ComplexInputDialog::accept() {
  QString real = _realLineEdit->text();
  int realIndex = 0;

  QString imag = _imaginaryLineEdit->text();
  int imagIndex = 0;

  if (_realValidator->validate(real, realIndex) == QValidator::Acceptable &&
      _imaginaryValidator->validate(imag, imagIndex) ==
          QValidator::Acceptable) {
    _complex.setReal(
        static_cast<MandelsetController::Type>(std::stold(real.toStdString())));
    _complex.setImaginary(
        static_cast<MandelsetController::Type>(std::stold(imag.toStdString())));
    done(Accepted);
  }
}

void ComplexInputDialog::setComplex(
    const Complex<MandelsetController::Type> &complex) {
  _complex = complex;
}

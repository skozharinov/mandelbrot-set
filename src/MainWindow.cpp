﻿#include "MainWindow.hpp"

#include "ComplexInputDialog.hpp"
#include "ZoomableLabel.hpp"
#include "IterationsInputDialog.hpp"
#include "Mandelset.hpp"
#include "RenderThread.hpp"
#include "Util.hpp"
#include "ZoomInputDialog.hpp"
#include <QAction>
#include <QActionGroup>
#include <QApplication>
#include <QFileDialog>
#include <QIcon>
#include <QImageWriter>
#include <QLabel>
#include <QMenuBar>
#include <QMessageBox>
#include <QMouseEvent>
#include <QPainter>
#include <QProgressDialog>
#include <QStandardPaths>
#include <QStatusBar>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent) {
  setWindowIcon(QIcon(":/images/icon.png"));
  setWindowTitle(QApplication::applicationDisplayName());
  setMinimumSize(400, 300);

  _pixmapLabel = new ZoomableLabel();
  setCentralWidget(_pixmapLabel);

  _zoomLabel = new QLabel();
  statusBar()->addPermanentWidget(_zoomLabel);

  _controller = new MandelsetController(this);

  _progressDialog = new QProgressDialog(this);
  _progressDialog->setWindowTitle(QApplication::applicationDisplayName());
  _progressDialog->setLabelText(tr("Rendering..."));
  _progressDialog->setWindowModality(Qt::WindowModal);
  _progressDialog->setCancelButtonText(tr("Cancel"));

  _renderThread = new RenderThread(_controller, this);

  connect(_renderThread, &RenderThread::rendered, this,
          &MainWindow::updatePixmap);
  connect(_renderThread, &RenderThread::started, this,
          &MainWindow::showProgressDialog);
  connect(_renderThread, &RenderThread::progress, this,
          &MainWindow::updateProgress);
  connect(_progressDialog, &QProgressDialog::canceled, _renderThread,
          &RenderThread::abort);

  createActions();
  createMenus();

  updateLabels();
}

MainWindow::~MainWindow() {
  _renderThread->abort();
  _renderThread->wait();
}

void MainWindow::exportImage() {
  QFileDialog fileDialog(this, tr("Export As"));
  fileDialog.setAcceptMode(QFileDialog::AcceptSave);
  fileDialog.setFileMode(QFileDialog::AnyFile);
  fileDialog.setDirectory(
      QStandardPaths::writableLocation(QStandardPaths::PicturesLocation));

  QStringList mimeTypes;
  const QList<QByteArray> baMimeTypes = QImageWriter::supportedMimeTypes();

  for (const QByteArray &bf : baMimeTypes)
    mimeTypes.append(QString(bf));

  fileDialog.setMimeTypeFilters(mimeTypes);
  fileDialog.selectMimeTypeFilter("image/png");
  fileDialog.setDefaultSuffix("png");

  if (fileDialog.exec() != QDialog::Accepted) {
    return;
  }

  const QString fileName = fileDialog.selectedFiles().first();
  if (!_pixmap.save(fileName)) {
    QMessageBox::warning(this, tr("Export Error"),
                         tr("The image could not be saved to \"%1\".")
                             .arg(QDir::toNativeSeparators(fileName)));
  }
}

void MainWindow::reset() {
  _controller->lock();

  _controller->setSize(Point(_pixmapLabel->width(), _pixmapLabel->height()));
  _controller->setZoom(MandelsetController::DefaultZoom);
  _controller->setCenter(MandelsetController::DefaultCenter);
  _controller->setPalette(MandelsetController::DefaultPalette);
  _controller->setIterations(MandelsetController::DefaultIterations);
  _controller->setSmooth(MandelsetController::DefaultSmooth);
  _controller->setHistogram(MandelsetController::DefaultHistogram);

  _paletteActions[MandelsetController::DefaultPalette]->setChecked(true);
  _smoothAction->setChecked(MandelsetController::DefaultSmooth);
  _histogramAction->setChecked(MandelsetController::DefaultHistogram);

  updateLabels();
  render();

  _controller->unlock();
}

void MainWindow::exit() { QApplication::quit(); }

void MainWindow::setCenter() {
  _controller->lock();

  ComplexInputDialog inputDialog(this);
  inputDialog.setComplex(_controller->center());

  if (inputDialog.exec() != QDialog::Accepted) {
    _controller->unlock();
    return;
  }

  if (!_controller->center().almostEquals(inputDialog.complex())) {
    _controller->setCenter(inputDialog.complex());

    updateLabels();
    render();
  }

  _controller->unlock();
}

void MainWindow::setZoom() {
  _controller->lock();

  ZoomInputDialog inputDialog(this);
  inputDialog.setZoom(_controller->zoom());

  if (inputDialog.exec() != QDialog::Accepted) {
    _controller->unlock();
    return;
  }

  if (_controller->zoom() != inputDialog.zoom()) {
    _controller->setZoom(inputDialog.zoom());

    updateLabels();
    render();
  }

  _controller->unlock();
}

void MainWindow::setIterations() {
  _controller->lock();

  IterationsInputDialog inputDialog(this);
  inputDialog.setIterations(_controller->iterations());

  if (inputDialog.exec() != QDialog::Accepted) {
    _controller->unlock();
    return;
  }

  if (_controller->iterations() != inputDialog.iterations()) {
    _controller->setIterations(inputDialog.iterations());

    updateLabels();
    render();
  }

  _controller->unlock();
}

void MainWindow::setPalette() {
  _controller->lock();

  for (auto it = _paletteActions.keyValueBegin();
       it != _paletteActions.keyValueEnd(); ++it) {
    if ((*it).second->isChecked()) {
      _controller->setPalette((*it).first);
      render();
      break;
    }
  }

  _controller->unlock();
}

void MainWindow::zoomIn() {
  _controller->lock();
  zoomTo(_controller->center(), 2);
  _controller->unlock();
}

void MainWindow::zoomOut() {
  _controller->lock();
  zoomTo(_controller->center(), 0.5);
  _controller->unlock();
}

void MainWindow::smooth() {
  _controller->lock();
  _controller->setSmooth(_smoothAction->isChecked());
  render();
  _controller->unlock();
}

void MainWindow::histogram() {
  _controller->lock();
  _controller->setHistogram(_histogramAction->isChecked());
  render();
  _controller->unlock();
}

void MainWindow::about() {
  QMessageBox::about(this, QApplication::applicationDisplayName(),
                     tr("A school project that visualizes Mandelbrot set on "
                        "complex plane."));
}

void MainWindow::aboutQt() { QMessageBox::aboutQt(this); }

void MainWindow::render() { _renderThread->render(); }

void MainWindow::zoomTo(Complex<MandelsetController::Type> center,
                        float scale) {
  assert(scale > 0);

  _controller->lock();

  if (_controller->zoom() * scale <= std::numeric_limits<uint64_t>::max() &&
      _controller->zoom() * scale >= 1) {
    _controller->setCenter(center);
    _controller->setZoom(static_cast<uint64_t>(_controller->zoom() * scale));

    updateLabels();
    render();
  }

  _controller->unlock();
}

void MainWindow::showProgressDialog() { _progressDialog->show(); }

void MainWindow::updateProgress(int value) { _progressDialog->setValue(value); }

void MainWindow::updateLabels() {
  _controller->lock();

  _zoomLabel->setText(tr("Zoom: %1x").arg(_controller->zoom()));

  _controller->unlock();
}

void MainWindow::updatePixmap(const QPixmap &pixmap) {
  _pixmap = pixmap;
  _pixmapLabel->setPixmap(_pixmap);
}

void MainWindow::createActions() {
  _exportAction = new QAction(tr("Export"), this);
  _exportAction->setIcon(QIcon::fromTheme("document-save-as"));
  _exportAction->setShortcut(QKeySequence::SaveAs);
  connect(_exportAction, &QAction::triggered, this, &MainWindow::exportImage);

  _resetAction = new QAction(tr("Reset"), this);
  _resetAction->setShortcut(QKeySequence::Refresh);
  connect(_resetAction, &QAction::triggered, this, &MainWindow::reset);

  _exitAction = new QAction(tr("Exit"), this);
  _exitAction->setIcon(QIcon::fromTheme("application-exit"));
  _exitAction->setShortcut(QKeySequence::Quit);
  connect(_exitAction, &QAction::triggered, this, &MainWindow::exit);

  _setCenterAction = new QAction(tr("Set Center"), this);
  connect(_setCenterAction, &QAction::triggered, this, &MainWindow::setCenter);

  _setZoomAction = new QAction(tr("Set Zoom"), this);
  connect(_setZoomAction, &QAction::triggered, this, &MainWindow::setZoom);

  _setIterationsAction = new QAction(tr("Set Iterations"), this);
  connect(_setIterationsAction, &QAction::triggered, this,
          &MainWindow::setIterations);

  _zoomInAction = new QAction(tr("Zoom In"), this);
  _zoomInAction->setIcon(QIcon::fromTheme("zoom-in"));
  _zoomInAction->setShortcut(QKeySequence::ZoomIn);
  connect(_zoomInAction, &QAction::triggered, this, &MainWindow::zoomIn);

  _zoomOutAction = new QAction(tr("Zoom Out"), this);
  _zoomOutAction->setIcon(QIcon::fromTheme("zoom-out"));
  _zoomOutAction->setShortcut(QKeySequence::ZoomOut);
  connect(_zoomOutAction, &QAction::triggered, this, &MainWindow::zoomOut);

  _smoothAction = new QAction(tr("Smooth Coloring"), this);
  _smoothAction->setCheckable(true);
  _smoothAction->setChecked(MandelsetController::DefaultSmooth);
  connect(_smoothAction, &QAction::triggered, this, &MainWindow::smooth);

  _histogramAction = new QAction(tr("Histogram Coloring"), this);
  _histogramAction->setCheckable(true);
  _histogramAction->setChecked(MandelsetController::DefaultHistogram);
  connect(_histogramAction, &QAction::triggered, this, &MainWindow::histogram);

  _purePaletteAction = new QAction(tr("Pure Palette"), this);
  _purePaletteAction->setCheckable(true);
  connect(_purePaletteAction, &QAction::triggered, this,
          &MainWindow::setPalette);

  _grayscalePaletteAction = new QAction(tr("Grayscale Palette"), this);
  _grayscalePaletteAction->setCheckable(true);
  connect(_grayscalePaletteAction, &QAction::triggered, this,
          &MainWindow::setPalette);

  _grayscaleRPaletteAction = new QAction(tr("Grayscale R Palette"), this);
  _grayscaleRPaletteAction->setCheckable(true);
  connect(_grayscaleRPaletteAction, &QAction::triggered, this,
          &MainWindow::setPalette);

  _bluePaletteAction = new QAction(tr("Blue Palette"), this);
  _bluePaletteAction->setCheckable(true);
  connect(_bluePaletteAction, &QAction::triggered, this,
          &MainWindow::setPalette);

  _royalPaletteAction = new QAction(tr("Royal Palette"), this);
  _royalPaletteAction->setCheckable(true);
  connect(_royalPaletteAction, &QAction::triggered, this,
          &MainWindow::setPalette);

  _huePaletteAction = new QAction(tr("Hue Palette"), this);
  _huePaletteAction->setCheckable(true);
  connect(_huePaletteAction, &QAction::triggered, this,
          &MainWindow::setPalette);

  _hueRPaletteAction = new QAction(tr("Hue R Palette"), this);
  _hueRPaletteAction->setCheckable(true);
  connect(_hueRPaletteAction, &QAction::triggered, this,
          &MainWindow::setPalette);

  _starRPaletteAction = new QAction(tr("Star R Palette"), this);
  _starRPaletteAction->setCheckable(true);
  connect(_starRPaletteAction, &QAction::triggered, this,
          &MainWindow::setPalette);

  _heartPaletteAction = new QAction(tr("Heart Palette"), this);
  _heartPaletteAction->setCheckable(true);
  connect(_heartPaletteAction, &QAction::triggered, this,
          &MainWindow::setPalette);

  _paletteActions.insert(Colorizer::Pure, _purePaletteAction);
  _paletteActions.insert(Colorizer::Grayscale, _grayscalePaletteAction);
  _paletteActions.insert(Colorizer::GrayscaleR, _grayscaleRPaletteAction);
  _paletteActions.insert(Colorizer::Blue, _bluePaletteAction);
  _paletteActions.insert(Colorizer::Royal, _royalPaletteAction);
  _paletteActions.insert(Colorizer::Hue, _huePaletteAction);
  _paletteActions.insert(Colorizer::HueR, _hueRPaletteAction);
  _paletteActions.insert(Colorizer::StarR, _starRPaletteAction);
  _paletteActions.insert(Colorizer::Heart, _heartPaletteAction);

  _paletteGroup = new QActionGroup(this);
  _paletteGroup->setExclusive(true);
  _paletteGroup->addAction(_purePaletteAction);
  _paletteGroup->addAction(_grayscalePaletteAction);
  _paletteGroup->addAction(_grayscaleRPaletteAction);
  _paletteGroup->addAction(_bluePaletteAction);
  _paletteGroup->addAction(_royalPaletteAction);
  _paletteGroup->addAction(_huePaletteAction);
  _paletteGroup->addAction(_hueRPaletteAction);
  _paletteGroup->addAction(_starRPaletteAction);
  _paletteGroup->addAction(_heartPaletteAction);

  _paletteActions[MandelsetController::DefaultPalette]->setChecked(true);

  _aboutAction = new QAction(tr("About"), this);
  _aboutAction->setIcon(QIcon::fromTheme("help-about"));
  connect(_aboutAction, &QAction::triggered, this, &MainWindow::about);

  _aboutQtAction = new QAction(tr("About Qt"), this);
  connect(_aboutQtAction, &QAction::triggered, this, &MainWindow::aboutQt);
}

void MainWindow::createMenus() {
  _fileMenu = menuBar()->addMenu(tr("File"));
  _fileMenu->addAction(_exportAction);
  _fileMenu->addSeparator();
  _fileMenu->addAction(_resetAction);
  _fileMenu->addSeparator();
  _fileMenu->addAction(_exitAction);

  _editMenu = menuBar()->addMenu(tr("Edit"));
  _editMenu->addAction(_setCenterAction);
  _editMenu->addAction(_setZoomAction);
  _editMenu->addAction(_setIterationsAction);

  _viewMenu = menuBar()->addMenu(tr("View"));
  _viewMenu->addAction(_zoomInAction);
  _viewMenu->addAction(_zoomOutAction);
  _viewMenu->addSeparator();
  _viewMenu->addAction(_smoothAction);
  _viewMenu->addAction(_histogramAction);
  _viewMenu->addSeparator();

  QMenu *_paletteMenu = _viewMenu->addMenu(tr("Palette"));

  for (auto &action : _paletteGroup->actions()) {
    _paletteMenu->addAction(action);
  }

  _helpMenu = menuBar()->addMenu(tr("Help"));
  _helpMenu->addAction(_aboutAction);
  _helpMenu->addAction(_aboutQtAction);
}

﻿#include "MainWindow.hpp"
#include <QApplication>
#include <QTranslator>

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);

    QTranslator translator;

    if (translator.load(QLocale(), QStringLiteral(),
                        QStringLiteral(), QStringLiteral(":/translations"))) {
        app.installTranslator(&translator);
    }

    app.setApplicationDisplayName(QApplication::tr("Mandelbrot Set"));

    MainWindow window;
    window.show();

    return app.exec();
}

﻿#include "UInt64Validator.hpp"

#include <limits>

UInt64Validator::UInt64Validator(QObject *parent)
    : UInt64Validator(std::numeric_limits<uint64_t>::min(),
                      std::numeric_limits<uint64_t>::max(), parent) {}

UInt64Validator::UInt64Validator(uint64_t bottom, uint64_t top, QObject *parent)
    : QValidator(parent), _bottom(bottom), _top(top) {}

QValidator::State UInt64Validator::validate(QString &text, int &) const {
  if (text.isEmpty()) {
    return Intermediate;
  }

  bool flag = false;
  uint64_t result = text.toULongLong(&flag);

  if (!flag) {
    return Invalid;
  } else if (result < _bottom || result > _top) {
    return Intermediate;
  } else {
    return Acceptable;
  }
}

void UInt64Validator::fixup(QString &text) const {
  for (QString::size_type i = 0; i < text.length(); ++i) {
    if (!text.at(i).isDigit()) {
      text.remove(i, 1);
    }
  }
}

uint64_t UInt64Validator::bottom() const { return _bottom; }

void UInt64Validator::setBottom(const uint64_t &bottom) { _bottom = bottom; }

uint64_t UInt64Validator::top() const { return _top; }

void UInt64Validator::setTop(const uint64_t &top) { _top = top; }

void UInt64Validator::setRange(const uint64_t &bottom, const uint64_t &top) {
  _bottom = bottom;
  _top = top;
}

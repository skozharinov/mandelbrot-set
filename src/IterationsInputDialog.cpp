﻿#include "IterationsInputDialog.hpp"

#include "UInt64Validator.hpp"
#include <QApplication>
#include <QFormLayout>
#include <QLineEdit>
#include <QPushButton>
#include <limits>

IterationsInputDialog::IterationsInputDialog(QWidget *parent)
    : QDialog(parent) {
  setWindowTitle(QApplication::applicationDisplayName());

  _iterationsValidator =
      new UInt64Validator(8, std::numeric_limits<uint16_t>::max(), this);

  _acceptButton = new QPushButton(tr("Enter"));
  connect(_acceptButton, &QPushButton::clicked, this, &QDialog::accept);

  _iterationsLineEdit = new QLineEdit();
  _iterationsLineEdit->setValidator(_iterationsValidator);

  QFormLayout *layout = new QFormLayout();
  layout->addRow(tr("Iterations"), _iterationsLineEdit);
  layout->addRow(_acceptButton);

  setLayout(layout);
}

uint16_t IterationsInputDialog::iterations() const { return _iterations; }

void IterationsInputDialog::accept() {
  QString text = _iterationsLineEdit->text();
  int index = 0;
  if (_iterationsValidator->validate(text, index) == QValidator::Acceptable) {
    _iterations = text.toUShort();
    done(Accepted);
  }
}

void IterationsInputDialog::setIterations(const uint16_t &iterations) {
  _iterations = iterations;
}

void IterationsInputDialog::showEvent(QShowEvent *event) {
  QDialog::showEvent(event);

  _iterationsLineEdit->setText(QString::number(_iterations));
}

#include "ZoomableLabel.hpp"

#include "Point.hpp"
#include "MainWindow.hpp"
#include "Mandelset.hpp"
#include <QResizeEvent>
#include <QMouseEvent>
#include <QPen>
#include <QPainter>
#include <QProgressDialog>
#include <QStatusBar>

void ZoomableLabel::resizeEvent(QResizeEvent *event) {
  auto *window = dynamic_cast<MainWindow *>(parent());
  if (window == nullptr) return;
  
  window->_controller->lock();
  
  if (event->oldSize() != event->size()) {
    Point size(width(), height());
    window->_controller->setSize(size);
    window->_progressDialog->setMaximum(size.x() * size.y());
    window->render();
    window->statusBar()->showMessage(tr("%1 x %2").arg(size.x()).arg(size.y()));
  }

  window->_controller->unlock();
}

void ZoomableLabel::mouseDoubleClickEvent(QMouseEvent *event) {
  auto *window = dynamic_cast<MainWindow *>(parent());
  if (window == nullptr) return;

  QPoint pos = event->pos();
  Point point(pos.x(), pos.y());
  Complex<MandelsetController::Type> center = window->_controller->set().scale(point);

  if (event->button() == Qt::LeftButton) {
    window->zoomTo(center, 2);
  } else if (event->button() == Qt::RightButton) {
    window->zoomTo(center, 0.5);
  }
}

void ZoomableLabel::mousePressEvent(QMouseEvent *event) {
  auto *window = dynamic_cast<MainWindow *>(parent());
  if (window == nullptr) return;

  window->_controller->lock();

  if (event->button() & (Qt::LeftButton | Qt::RightButton)) {
    _source = event->pos();

    Point source(_source.x(), _source.y());
    Complex<MandelsetController::Type> c = window->_controller->set().scale(source);
    QString real = QString::fromStdString(Util::toStdString(c.real(), 16));
    QString imag = QString::fromStdString(Util::toStdString(c.imaginary(), 16));
    window->statusBar()->showMessage(tr("(%1; %2)").arg(real).arg(imag));
  }

  window->_controller->unlock();
}

void ZoomableLabel::mouseMoveEvent(QMouseEvent *event) {
  auto *window = dynamic_cast<MainWindow *>(parent());
  if (window == nullptr) return;

  window->_controller->lock();

  if (event->buttons() & (Qt::LeftButton | Qt::RightButton)) {
    QPoint destination = event->pos();

    QPixmap pixmap = window->_pixmap;
    QPainter painter(&pixmap);

    if (event->buttons() & Qt::LeftButton) {
      painter.setPen(QPen(Qt::green, 1, Qt::SolidLine));
    } else if (event->buttons() & Qt::RightButton) {
      painter.setPen(QPen(Qt::red, 1, Qt::SolidLine));
    }

    painter.drawRect(QRect(_source, destination).normalized());
    painter.end();
    setPixmap(pixmap);
  }

  window->_controller->unlock();
}

void ZoomableLabel::mouseReleaseEvent(QMouseEvent *event) {
  auto *window = dynamic_cast<MainWindow *>(parent());
  if (window == nullptr) return;
  
  window->_controller->lock();

  if (event->button() & (Qt::LeftButton | Qt::RightButton)) {
    QPoint _destination = event->pos();
    setPixmap(window->_pixmap);

    if (_destination != _source) {
      Mandelset<MandelsetController::Type> set = window->_controller->set();
      Point source(_source.x(), _source.y());
      Point destination(_destination.x(), _destination.y());
      Point delta = destination - source;
      Point center(source.x() + delta.x() / 2, source.y() + delta.y() / 2);
      Point size = set.size();
      float scale = qMin(qAbs(static_cast<float>(size.x()) / delta.x()),
                         qAbs(static_cast<float>(size.y()) / delta.y()));

      if (event->button() & Qt::LeftButton) {
        window->zoomTo(set.scale(center), scale);
      } else if (event->button() & Qt::RightButton) {
        window->zoomTo(set.scale(center), 1 / scale);
      }
    }
  }

  window->_controller->unlock();
}


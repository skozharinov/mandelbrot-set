﻿#include "MandelsetController.hpp"
#include "Mandelset.hpp"

MandelsetController::MandelsetController(QObject *parent)
    : QObject(parent), _mutex(QMutex::Recursive), _center(DefaultCenter),
      _size(DefaultSize), _zoom(DefaultZoom), _iterations(DefaultIterations),
      _palette(DefaultPalette), _smooth(DefaultSmooth),
      _histogram(DefaultHistogram) {
  save();
}

Mandelset<MandelsetController::Type> MandelsetController::set() {
  return {_size, _zoom, _center, _iterations};
}

Colorizer::Palette MandelsetController::palette() { return _palette; }

void MandelsetController::setPalette(const Colorizer::Palette &palette) {
  _palette = palette;
}

bool MandelsetController::smooth() { return _smooth; }

void MandelsetController::setSmooth(bool smooth) { _smooth = smooth; }

void MandelsetController::lock() { _mutex.lock(); }

void MandelsetController::unlock() { _mutex.unlock(); }

void MandelsetController::save() {
  _mutex.lock();
  _savedCenter = _center;
  _savedSize = _size;
  _savedZoom = _zoom;
  _savedIterations = _iterations;
  _savedPalette = _palette;
  _savedSmooth = _smooth;
  _savedHistogram = _histogram;
  _mutex.unlock();
}

void MandelsetController::restore() {
  _mutex.lock();
  _center = _savedCenter;
  _size = _savedSize;
  _zoom = _savedZoom;
  _iterations = _savedIterations;
  _palette = _savedPalette;
  _smooth = _savedSmooth;
  _histogram = _savedHistogram;
  _mutex.unlock();
}

bool MandelsetController::histogram() const { return _histogram; }

void MandelsetController::setHistogram(bool histogram) {
  _histogram = histogram;
}

Complex<MandelsetController::Type> MandelsetController::center() {
  return _center;
}

void MandelsetController::setCenter(const Complex<Type> &center) {
  _center = center;
}

Point MandelsetController::size() { return _size; }

void MandelsetController::setSize(const Point &size) { _size = size; }

uint64_t MandelsetController::zoom() { return _zoom; }

void MandelsetController::setZoom(const uint64_t &zoom) { _zoom = zoom; }

uint16_t MandelsetController::iterations() { return _iterations; }

void MandelsetController::setIterations(const uint16_t &iterations) {
  _iterations = iterations;
}

constexpr const Complex<MandelsetController::Type>
    MandelsetController::DefaultCenter;
constexpr const Point MandelsetController::DefaultSize;
constexpr const uint64_t MandelsetController::DefaultZoom;
constexpr const uint16_t MandelsetController::DefaultIterations;
constexpr const Colorizer::Palette MandelsetController::DefaultPalette;
constexpr const bool MandelsetController::DefaultSmooth;
constexpr const bool MandelsetController::DefaultHistogram;

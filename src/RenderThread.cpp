﻿#include "RenderThread.hpp"

#include "HsvColor.hpp"
#include "Mandelset.hpp"
#include "MandelsetController.hpp"
#include <QImage>
#include <QPixmap>
#include <cmath>

RenderThread::RenderThread(MandelsetController *controller, QObject *parent)
    : QThread(parent) {
  _controller = controller;
  _restart = false;
  _abort = false;
}

void RenderThread::render() {
  if (isRunning()) {
    restart();
  } else {
    start();
  }
}

void RenderThread::restart() {
  _mutex.lock();

  if (isRunning()) {
    _restart = true;
  }

  _mutex.unlock();
}

void RenderThread::abort() {
  _mutex.lock();

  if (isRunning()) {
    _abort = true;
  }

  _mutex.unlock();
}

void RenderThread::run() {
  checkRestart();
  checkAbort();

  while (true) {
    _controller->lock();
    const Mandelset<MandelsetController::Type> set = _controller->set();
    const Colorizer::Palette palette = _controller->palette();
    const bool smooth = _controller->smooth();
    const bool histogram = _controller->histogram();
    _controller->unlock();

    QImage result(set.size().x(), set.size().y(), QImage::Format_RGB32);
    const Colorizer colorizer = Colorizer(palette);
    bool restartFlag = false;

    if (histogram) {
      QVector<float> values(set.size().x() * set.size().y());
      QVector<uint32_t> hist(set.iterations());

      for (int32_t x = 0; x < set.size().x(); ++x) {
        for (int32_t y = 0; y < set.size().y(); ++y) {
          if (smooth) {
            const float iterations = set.smoothIterations(Point(x, y));
            const float max = static_cast<float>(set.iterations());
            values[x * set.size().y() + y] = iterations;

            if (!Util::almostEquals(iterations, max)) {
              hist[static_cast<int32_t>(iterations)]++;
            }
          } else {
            const uint16_t iterations = set.iterations(Point(x, y));
            values[x * set.size().y() + y] = iterations;

            if (iterations != set.iterations()) {
              hist[iterations]++;
            }
          }
        }

        restartFlag = checkRestart();

        if (restartFlag) {
          break;
        } else if (checkAbort()) {
          _controller->restore();
          return;
        }

        emit progress(x * set.size().y() / 2);
      }

      float total = 0;

      for (uint16_t i = 0; i < set.iterations(); ++i) {
        total += hist[i];
      }

      QVector<float> hues(set.iterations() + 1);
      float h = 0;

      for (uint16_t i = 0; i < set.iterations(); ++i) {
        h += hist[i] / total;
        hues[i] = std::min(1.0f, h);
      }

      for (int32_t x = 0; x < set.size().x(); ++x) {
        for (int32_t y = 0; y < set.size().y(); ++y) {
          if (smooth) {
            const float iterations = values[x * set.size().y() + y];
            const float max = static_cast<float>(set.iterations());

            if (Util::almostEquals(iterations, max)) {
              const RgbColor color = colorizer(iterations / max);
              result.setPixel(x, y, color.toRGB32());
            } else {
              const float fraction = std::fmod(iterations, 1.0f);
              const float hue1 = hues[static_cast<int32_t>(iterations)];
              const float hue2 = hues[static_cast<int32_t>(iterations) + 1];
              const RgbColor color1 = colorizer(hue1);
              const RgbColor color2 = colorizer(hue2);
              const RgbColor color = color1.interpolate(color2, fraction);
              result.setPixel(x, y, color.toRGB32());
            }
          } else {
            const float iterations = values[x * set.size().y() + y];

            if (iterations == set.iterations()) {
              const RgbColor color = colorizer(iterations / set.iterations());
              result.setPixel(x, y, color.toRGB32());
            } else {
              const float hue = hues[static_cast<uint16_t>(iterations)];
              const RgbColor color = colorizer(hue);
              result.setPixel(x, y, color.toRGB32());
            }
          }
        }

        restartFlag = checkRestart();

        if (restartFlag) {
          break;
        } else if (checkAbort()) {
          _controller->restore();
          return;
        }

        emit progress((set.size().x() + x) * set.size().y() / 2);
      }

      emit progress(set.size().x() * set.size().y());
    } else {
      for (int32_t x = 0; x < set.size().x(); ++x) {
        for (int32_t y = 0; y < set.size().y(); ++y) {
          if (smooth) {
            const float iterations = set.smoothIterations(Point(x, y));
            const RgbColor color = colorizer(iterations / set.iterations());
            result.setPixel(x, y, color.toRGB32());
          } else {
            const float iterations = set.iterations(Point(x, y));
            const RgbColor color = colorizer(iterations / set.iterations());
            result.setPixel(x, y, color.toRGB32());
          }
        }

        restartFlag = checkRestart();

        if (restartFlag) {
          break;
        } else if (checkAbort()) {
          _controller->restore();
          return;
        }

        emit progress(x * set.size().y());
      }

      emit progress(set.size().x() * set.size().y());
    }

    if (!restartFlag && !checkRestart()) {
      _controller->save();
      emit rendered(QPixmap::fromImage(result));
      break;
    }
  }
}

bool RenderThread::checkRestart() {
  _mutex.lock();
  const bool result = _restart;
  _restart = false;
  _mutex.unlock();
  return result;
}

bool RenderThread::checkAbort() {
  _mutex.lock();
  const bool result = _abort;
  _abort = false;
  _mutex.unlock();
  return result;
}

﻿#include "ZoomInputDialog.hpp"

#include "UInt64Validator.hpp"
#include <QApplication>
#include <QFormLayout>
#include <QLineEdit>
#include <QPushButton>
#include <limits>

ZoomInputDialog::ZoomInputDialog(QWidget *parent) : QDialog(parent) {
  setWindowTitle(QApplication::applicationDisplayName());

  _zoomValidator =
      new UInt64Validator(1, std::numeric_limits<uint64_t>::max(), this);

  _acceptButton = new QPushButton(tr("Enter"));
  connect(_acceptButton, &QPushButton::clicked, this, &QDialog::accept);

  _zoomLineEdit = new QLineEdit();
  _zoomLineEdit->setValidator(_zoomValidator);

  QFormLayout *layout = new QFormLayout();
  layout->addRow(tr("Zoom"), _zoomLineEdit);
  layout->addRow(_acceptButton);

  setLayout(layout);
}

uint64_t ZoomInputDialog::zoom() const { return _zoom; }

void ZoomInputDialog::accept() {
  QString text = _zoomLineEdit->text();
  int index = 0;
  if (_zoomValidator->validate(text, index) == QValidator::Acceptable) {
    _zoom = text.toULongLong();
    done(Accepted);
  }
}

void ZoomInputDialog::setZoom(const uint64_t &zoom) { _zoom = zoom; }

void ZoomInputDialog::showEvent(QShowEvent *event) {
  QDialog::showEvent(event);

  _zoomLineEdit->setText(QString::number(_zoom));
}
